
clc;clear all;close all;
%% Define Channel Variables
L = 3;
NAnt = [16]; %Num of Antennas
fc = 2e9; fs = fc*10;
c = 3e8;
lambda = c/fc;
d = lambda/2;      % traditional antenna placing λ/2 for fc = 2GHz
eta =pi; %%--> assume to be pi
minSNR = 1;
maxSNR = 30;
SNRs = minSNR:1:maxSNR;
nSNR_level = length(SNRs);
rho_UL = 200;
coeff = sqrt(rho_UL/L);

%% Define Environment Setup
M = 10; % 10 APs
K = 20; % 20 users
AoA = sort([pi/2 pi/4 pi/10],'descend');
AoA = AoA';

%% ==== Get ComputeCanada JobID ======
job=getenv('SLURM_JOB_ID');
fileID = fopen(strcat('exp_out.txt',job),'w');

%% To see how varying number of antennas results
numAntenna = length(NAnt);
ant_snr_RMSE_Phi_dft            = zeros(numAntenna, nSNR_level, 1);
ant_snr_RMSE_Phi_matpencil      = zeros(numAntenna, nSNR_level, 1);
ant_snr_RMSE_Gamma              = zeros(numAntenna, nSNR_level, L);
ant_snr_RMSE_Gamma_matpencil    = zeros(numAntenna, nSNR_level, L);
ant_snr_RMSE_Beta              = zeros(numAntenna, nSNR_level, L);
ant_snr_RMSE_Beta_matpencil    = zeros(numAntenna, nSNR_level, L);
ant_snr_RMSE_Hmk              = zeros(numAntenna, nSNR_level, 1);
ant_snr_RMSE_Hmk_matpencil    = zeros(numAntenna, nSNR_level, 1);
iterNAnt = 1;
N = NAnt(iterNAnt);
n = linspace(0,N-1,N);
% ******
% Simulation Scenarios: 1 AP <-> 1 User: 3 Propagation Paths
% ******
%% ====  Init Channel params ====

% Uplink Pilot Signals
% pilot_UL = L x tau
tau = K; % coherence time
pilot_UL = ones(1,tau).*exp(1i*(2*pi*fc*repmat([1:tau]/fs, 1, 1)));
pilot_UL = pilot_UL ./ norm(pilot_UL,2);


%% ============== Channel Estimation =========
gSize = 100;
G = linspace(-pi/N, pi/N, gSize);
%% ======== Define Num of Monte-Carlo Trials =========
nChannelSim = 100;
fprintf(fileID,'======== Start of Simulations =======\n');

%% ====== Declare Channel Perf. Vars ======
snr_RMSE_Phi_dft        = zeros(nSNR_level,1);
snr_RMSE_Phi_matpencil  = zeros(nSNR_level,1);
snr_RMSE_Gamma          = zeros(nSNR_level,L);
snr_RMSE_Gamma_matpencil         = zeros(nSNR_level,L);
snr_RMSE_Beta                    = zeros(nSNR_level,L);
snr_RMSE_Beta_matpencil          = zeros(nSNR_level,L);
snr_RMSE_Hmk                    = zeros(nSNR_level,1);
snr_RMSE_Hmk_matpencil          = zeros(nSNR_level,1);
% Num of Samples in Time
T = 16;
Bmk = genLargeScaleFading(L);

y_Samples = zeros(N,11);
idx_y_Samples = 1;
%% ====== Monte-Carlo SNR Sims =====
for iterSNR = 1:nSNR_level


    % Steering Vector
    for l=1:L
        Amk(:,l) = exp(1i.*n*eta.* sin(AoA(l)))./sqrt(N);
    end
    currSNR = SNRs(iterSNR); gamma = sqrt(10^(currSNR/20));
    Amk_amp = coeff*gamma*Amk;
    Amk_coeff = [];
    for p=1:L
        Amk_coeff(:,p) = Amk_amp(:,p) ./ 10^(p-1);
    end
    alpha = genSmallScaleFading(L);
    hmk = Amk_coeff*Bmk*diag(alpha);
    perf_RMSE_Phi_dft           = 0;
    perf_RMSE_Phi_matpencil     = 0;
    perf_RMSE_Gamma             = zeros(L,1);
    perf_RMSE_Gamma_matpencil   = zeros(L,1);
    perf_RMSE_Beta             = zeros(L,1);
    perf_RMSE_Beta_matpencil   = zeros(L,1);
    perf_RMSE_Hmk             = 0;
    perf_RMSE_Hmk_matpencil   = 0;


    PSig = 0;
    PNoise = 0;
    for iS = 1:nChannelSim
        alpha = genSmallScaleFading(L);
        nmk = genAWGNCh(N,K,L);
        %% === Sampling ===
        % Amk x Bmk x alpha:    (N,L)x(L,L)x(L,1)
        % pilot_UL:             1 x tau
        % hmk_Sampled:          N x tau
        % nmk_Sampled:          N x tau
        hmk = Amk_coeff*Bmk*diag(alpha);
        hmk_Sampled = sum(hmk,2)* pilot_UL;
        nmk_Sampled = sum(nmk,3);
        hmk_processed = hmk_Sampled(:,1:T)*(pilot_UL(1:T)');
        nmk_processed = nmk(:,1:T)*(pilot_UL(1:T)');

        ymk_Sampled = hmk_processed + nmk_processed;
        if (currSNR == 30)
            y_Samples(:,idx_y_Samples) = ymk_Sampled(:,1);
            idx_y_Samples = idx_y_Samples + 1;
        end

        %% Angle of Arrival Estimation
        AoA_deg = rad2deg(sort(AoA,'descend'));
        [AoA_dft_deg] =  dft_aoa(ymk_Sampled,N,L);
        [AoA_matpencil_deg] = matpencil_aoa(ymk_Sampled,L);

        %% Channel Amplitude Estimation
        Amk_hat = zeros(N,L);
        for p=1:L
            Amk_hat(:,p) = exp(-1i.*n'*eta*sind(AoA_dft_deg(p)))./sqrt(N);
        end

        %% Channel Amplitude Estimation
        Amk_hat_matpencil = zeros(N,L);
        for p=1:L
            Amk_hat_matpencil(:,p) = exp(-1i.*n'*eta*sind(AoA_matpencil_deg(p)))./sqrt(N);
        end
        Dmk_hat = sqrt(L/rho_UL)*pinv(Amk_hat'*Amk_hat)*Amk_hat'*ymk_Sampled;
        Rd_hat = 1/T*((Dmk_hat)*(Dmk_hat'));
        Bmk_hat = sort(diag(sqrt(Rd_hat)),'descend');

        Dmk_hat_matpencil = sqrt(L/rho_UL)*pinv(Amk_hat_matpencil'*Amk_hat_matpencil)*Amk_hat_matpencil'*ymk_Sampled;
        Rd_hat_matpencil = 1/T*((Dmk_hat_matpencil)*(Dmk_hat_matpencil'));
        Bmk_hat_matpencil = sort(diag(sqrt(Rd_hat_matpencil)),'descend');

        %% Performance Evaluation
        [RMSE_Phi_dft, RMSE_Bmk, RMSE_Gamma, RMSE_Hmk] = CSI_perfEval(AoA_deg,AoA_dft_deg,Amk,Amk_hat,Bmk,Bmk_hat,alpha);
        [RMSE_Phi_matpencil,RMSE_Bmk_matpencil, RMSE_Gamma_matpencil,RMSE_Hmk_matpencil] = CSI_perfEval(AoA_deg,AoA_matpencil_deg,Amk,Amk_hat_matpencil,Bmk,Bmk_hat_matpencil,alpha);

        perf_RMSE_Phi_dft = perf_RMSE_Phi_dft + RMSE_Phi_dft;
        perf_RMSE_Phi_matpencil = perf_RMSE_Phi_matpencil + RMSE_Phi_matpencil;
        perf_RMSE_Gamma = perf_RMSE_Gamma + RMSE_Gamma;
        perf_RMSE_Gamma_matpencil = perf_RMSE_Gamma_matpencil + RMSE_Gamma_matpencil;
        perf_RMSE_Beta = perf_RMSE_Beta + RMSE_Bmk;
        perf_RMSE_Beta_matpencil = perf_RMSE_Beta_matpencil + RMSE_Bmk_matpencil;
        perf_RMSE_Hmk = perf_RMSE_Hmk + RMSE_Hmk;
        perf_RMSE_Hmk_matpencil = perf_RMSE_Hmk_matpencil + RMSE_Hmk_matpencil;
        PSig = PSig + norm(hmk_processed,2);
        PNoise = PNoise + norm(nmk_processed,2);
    end

    avg_RMSE_Phi_dft = perf_RMSE_Phi_dft / nChannelSim;
    avg_RMSE_Phi_matpencil = perf_RMSE_Phi_matpencil / nChannelSim;
    avg_RMSE_Gamma = perf_RMSE_Gamma ./ nChannelSim;
    avg_RMSE_Gamma_matpencil = perf_RMSE_Gamma_matpencil ./ nChannelSim;
    avg_RMSE_Beta = perf_RMSE_Beta ./ nChannelSim;
    avg_RMSE_Beta_matpencil = perf_RMSE_Beta_matpencil ./ nChannelSim;
    avg_RMSE_Hmk = perf_RMSE_Hmk ./ nChannelSim;
    avg_RMSE_Hmk_matpencil = perf_RMSE_Hmk_matpencil ./ nChannelSim;

    %% ====== Recording Perf. for each SNR level =======
    snr_RMSE_Phi_dft(iterSNR) = sqrt(avg_RMSE_Phi_dft/3);
    snr_RMSE_Phi_matpencil(iterSNR) = sqrt(avg_RMSE_Phi_matpencil/3);

    snr_RMSE_Gamma(iterSNR,:) = sqrt(avg_RMSE_Gamma/3);
    snr_RMSE_Gamma_matpencil(iterSNR,:) = sqrt(avg_RMSE_Gamma_matpencil/3);

    snr_RMSE_Beta(iterSNR,:) = sqrt(avg_RMSE_Beta/3);
    snr_RMSE_Beta_matpencil(iterSNR,:) = sqrt(avg_RMSE_Beta_matpencil/3);

    snr_RMSE_Hmk(iterSNR,:) = sqrt(avg_RMSE_Hmk/3);
    snr_RMSE_Hmk_matpencil(iterSNR,:) = sqrt(avg_RMSE_Hmk_matpencil/3);

    %% Print Result to File
    fprintf(fileID,"----- SNR: %d iS %d -----\n",currSNR,iS);
    fprintf(fileID,"Averaged Signal Norm: %f\t Noise Norm: %f\n",PSig/nChannelSim,PNoise/nChannelSim);
    fprintf(fileID,"snr %d: snr_RMSE_Phi_dft: %f\n", currSNR, snr_RMSE_Phi_dft(iterSNR));
    fprintf(fileID,"snr %d: snr_RMSE_Phi_matpencil: %f\n", currSNR, snr_RMSE_Phi_matpencil(iterSNR));
    for p=1:L
        fprintf(fileID,"snr %d: snr_RMSE_Gamma: %f\n", currSNR, snr_RMSE_Gamma(iterSNR,p));
        fprintf(fileID,"snr %d: snr_RMSE_Gamma_matpencil: %f\n", currSNR, snr_RMSE_Gamma_matpencil(iterSNR,p));
    end


end

ant_snr_RMSE_Phi_dft(iterNAnt,:,1)             = sqrt(snr_RMSE_Phi_dft(:,1));
ant_snr_RMSE_Phi_matpencil(iterNAnt,:,1)       = sqrt(snr_RMSE_Phi_matpencil(:,1));
for p=1:L
    ant_snr_RMSE_Beta(iterNAnt,:,p)               = snr_RMSE_Beta(:,p);
    ant_snr_RMSE_Beta_matpencil(iterNAnt,:,p)     = snr_RMSE_Beta_matpencil(:,p);
    ant_snr_RMSE_Gamma(iterNAnt,:,p)               = snr_RMSE_Gamma(:,p);
    ant_snr_RMSE_Gamma_matpencil(iterNAnt,:,p)     = snr_RMSE_Gamma_matpencil(:,p);
end
ant_snr_RMSE_Hmk(iterNAnt,:)               = snr_RMSE_Hmk(:,1);
ant_snr_RMSE_Hmk_matpencil(iterNAnt,:)     = snr_RMSE_Hmk_matpencil(:,1);

%% ===  Plot Antenna ~ SNR vs AoA Estimation  (dB)  ===
iterNAnt = 1;
for N=NAnt
    fig = figure
    xlabel("SNR")
    ylabel("RMSE of Angle Estimation $(^o)$",'Interpreter','latex')
    hold on
    plot(SNRs, ant_snr_RMSE_Phi_dft(iterNAnt,:,1),'DisplayName', 'DFT');
    plot(SNRs, ant_snr_RMSE_Phi_matpencil(iterNAnt,:,1),'DisplayName', 'Matrix-Pencil');
    grid on
    title(sprintf("Performance Comparison on Angle Estimation (linear) NAnt=%d",N))
    legend show
    hold off
    pngfile=strcat('snr_RMSE_Phi_Ant',num2str(N),'_',job)
    print(fig,pngfile,'-dpng')
    iterNAnt = iterNAnt + 1;
end

fclose(fileID);

for p=1:L
    fig = figure();
    xlabel("SNR")
    ylabel("RMSE of Channel Estimation (dB)")
    hold on
    ii = 1;
    for N=NAnt
        plot(SNRs, 10*log10(ant_snr_RMSE_Gamma(ii,:,p)),'-*','DisplayName', sprintf("DFT--N Ant.=%d", N));
        ii = ii + 1;
    end
    ii = 1;
    for N=NAnt
        plot(SNRs, 10*log10(ant_snr_RMSE_Gamma_matpencil(ii,:,p)),'DisplayName', sprintf("Matrix-Pencil--N Ant.=%d", N));
        ii = ii + 1;
    end
    grid on
    title_str = ['Channel Est. Scattered Prop. Path ',num2str(p),'-Normalized $\frac{E_s}{N_0}=$', num2str(10^-(p-1))];
    title(title_str,'Interpreter','latex')
    legend show
    hold off
    pngfile=sprintf('snr_RMSE_dB_OverallChan_p%d_%s',p,job)
    print(fig,pngfile,'-dpng')
end

%       ===  Plot Antenna ~ SNR vs Large-Scale-Fading (dB). ===
fig = figure();
xlabel("SNR")
ylabel("RMSE of Channel Estimation (dB)")
hold on
ii = 1;
for N=NAnt
    plot(SNRs, 10*log10(ant_snr_RMSE_Hmk(ii,:,1)),'-*','DisplayName', sprintf("DFT--N Ant.=%d", N));
    ii = ii + 1;
end
ii = 1;
for N=NAnt
    plot(SNRs, 10*log10(ant_snr_RMSE_Hmk_matpencil(ii,:,1)),'DisplayName', sprintf("Matrix-Pencil--N Ant.=%d", N));
    ii = ii + 1;
end

[minval,~] = min(10*log10(ant_snr_RMSE_Hmk(1,:,1)));
[maxval,~] = max(10*log10(ant_snr_RMSE_Hmk(1,:,1)));
stdval = (maxval - minval);
ylim([minval-2*stdval, maxval+2*stdval])

grid on
title_str = ['Channel Est. between Single User and Single DU'];
title(title_str,'Interpreter','latex')
legend show
hold off
pngfile=sprintf('snr_RMSE_dB_Hmk_%s',job)
print(fig,pngfile,'-dpng')




function varargout = CSI_perfEval(AoA,AoA_hat,Amk,Amk_hat,Bmk,Bmk_hat,alpha)
%% RMSE for Estimated Angle

RMSE_Phi = sum(abs(AoA - AoA_hat).^2);

varargout{1} = RMSE_Phi;
%% RMSE for Estimated Amplitude
if nargin > 2
    % Amk:      NxL
    % Bmk:      LxL
    % alpha:    Lx1
    % Bmk_hat:  Lx1
    [N,L] = size(Amk);
    true_beta = diag(Bmk);
    est_beta = Bmk_hat;
    norm_beta = abs(Bmk_hat).^2;
    diff_beta = (true_beta - est_beta).^2;
    nrmse_beta = diff_beta ./ est_beta;
    varargout{2} = nrmse_beta;
    true_Gamma = Amk*Bmk*diag(alpha);
    est_Gamma = Amk_hat*diag(Bmk_hat);
    diff_Gamma = sum(abs(true_Gamma - est_Gamma).^2) ./ N;
    norm_Gamma = sum( (abs(est_Gamma)).^2) ./ N;
    nmse_Gamma = diff_Gamma ./ norm_Gamma;
    varargout{3} = nmse_Gamma';

    true_Hmk = sum(true_Gamma,2);
    est_Hmk = sum(est_Gamma,2);
    norm_Hmk = sum((abs(est_Hmk)).^2) ./ N;
    diff_Hmk = sum(abs(true_Hmk - est_Hmk).^2) ./ N;
    nmse_Hmk = diff_Hmk ./ norm_Hmk;
    varargout{4} = nmse_Hmk;
end
end


function alpha=genSmallScaleFading(L)
alpha = sqrt(1/2).*(randn(L,1) + 1i.*randn(L,1));
alpha = sort(alpha,'descend');
end

%% Large-scale Fading -- log-normal model
% For Current Simulation, one AP <-> one User
function Bmk = genLargeScaleFading(L)
sigma = 8;
d = rand(L,1)*0.4;
PL = -112.4271 - 38.*log10(d);
B_LS = sort(10.^(PL./10),'descend');
Bmk = diag(sqrt(B_LS));
end

function nmk = genAWGNCh(N,K,L)
% N = number of antenna
% K = number of users / length of pilot symbols
% Bandwidth = 100 MHz
power = 4e-07; % Noise power = -114 + 10*log10(0.1*10^9)= -34dBm
nmk = sqrt(power)*(randn(N,K,L) +1i*randn(N,K,L))./(sqrt(2));
end



function [Q,isNeg] = findInitDFTBin(hDFT,N,L)
[~,I] = sort(abs(hDFT),'descend');
threshold = floor(N/2);
Q = zeros(1,L);
isNeg = zeros(1,L);
pl = 1;

for l=1:N
    if I(l) > threshold
        Q(pl) = I(l);
        pl = pl+1;
        isNeg(l) = 1;
    else
        Q(pl) = I(l);
        pl = pl+1;
        isNeg(l) = 0;
    end
    if pl > L
        break;
    end
end

end

%% ===== Extended DFT + Angle-Rotation =====
function [AoA_Est_DFT] = dft_aoa(ymk_Sampled,N,L)
% Input:    yt, NumRx, N_MultiPath
% Output:   AOA_estimated, beta_estimated
FN = dftmtx(N)/sqrt(N);
Ndft_points = 1000; %% can choose whatever # you want

AoA_Est_DFT = zeros(L,1);
hDFT = FN * ymk_Sampled;

% Coarse Peak Finding
% -- Find the central point (qInit) of each path
[qInits,isNeg] = findInitDFTBin(hDFT,N,L);

for l=1:L
    fNq = FN(:,qInits(l));
    ymk_DFT = fNq .* ymk_Sampled;

    angles_in_phi = [-Ndft_points/2: Ndft_points /2]*pi/ Ndft_points; %% Ndft_points in the phi domain
    st_vec_mtx = exp(1i* [0:N-1]' * angles_in_phi);  %% N \times Ndft_points matrix of Ndft_points steering vectors

    % Now if x is the data vector
    angle_info = abs(st_vec_mtx' * ymk_DFT);
    [~, max_angle_location] = max(angle_info);
    phi_location = angles_in_phi(max_angle_location);
    theta_init = 2*qInits(l)/N;

    if isNeg(l)
        theta = -theta_init + phi_location/pi; %% since \phi = kd\sin\theta
    else
        theta = theta_init - phi_location/pi;
    end

    if abs(theta) > 1
        theta = findNextPeak(theta,qInits(l),angle_info,angles_in_phi,N);
    end

    AoA_Est_DFT(l) = rad2deg(theta);
end
AoA_Est_DFT = sort(AoA_Est_DFT);
end

function new_theta = findNextPeak(prev_theta,curr_qInit,angle_info,angles_in_phi,N)

[ang_pks,ang_loc] = findpeaks(angle_info);
[~,sorted_ang_loc_ind] = sort(ang_pks,'descend');
ang_loc_sorted = ang_loc(sorted_ang_loc_ind);

ang_locs_L = ang_loc_sorted(2:end);
isNeg = 0;
new_theta = 2; % dummy init value
if sign(prev_theta) < 1
    isNeg = 1;
end
idx=1; NPks = length(ang_locs_L);
while (abs(new_theta) > 1 && idx <= NPks)
    curr_max_angle_loc = ang_locs_L(idx);
    curr_phi_loc = angles_in_phi(curr_max_angle_loc);
    if isNeg
        new_theta = -2*curr_qInit/N + curr_phi_loc/pi;
    else
        new_theta = 2*curr_qInit/N - curr_phi_loc/pi;
    end
    idx = idx+1;
end
end


function DOAs = music_doa(ymk_Sampled,N,L,plot_spec)
%% AoA estimation using MUSIC
% ymk_Sampled = Nx1
% y = As + n
% N: Number of array elements
% L: Number of paths
ymk_Sampled = ymk_Sampled(:,1);
Ry = 1/N*(ymk_Sampled*ymk_Sampled'); %sample correlation matrix

[~,E,V] = svd(Ry);
[~, index]= sort(E,'descend');
V_sorted = V(index);
Vn = V_sorted(:,L+1:end);
theta = 0:1:180;
Pmusic = zeros(length(theta),1);
for i=1:length(theta)
    aa = exp(-1i*pi*sind(theta(i)).*([0:1:N-1]'));%Nx1
    Pmusic(i) = 1/(aa'*(Vn*Vn')*aa);
end
[Pmusic_sorted,index] = sort(real(10*log10(Pmusic)),'descend');
pk_locs = theta(index);
%% Choose the top L peaks
DOAs = sort(pk_locs(1:L),'descend');

end



%% ====== AoA estimation using Matrix Pencil ======
function AOAs = matpencil_aoa(ymk_Sampled,L)
% ymk_Sampled = Nx1
% y = As + n
% N: Number of array elements
% M: Number of paths
% L: Matrix Pencil parameter
P = 6; % size of window
[N,~] = size(ymk_Sampled);
assert( (L <= P) && (P<=(N-L)) );

x = ymk_Sampled(:,1); % Kx1=>only one time sample
Y1 = zeros(N-P,P);
Y2 = zeros(N-P,P);
for p=1:P
    Y1(:,p) = x(p:N-P-1+p,1);
    Y2(:,p) = x(p+1:N-P+p,1);
end

Y1_pinv = (Y1'*Y1)\Y1';
z_hat = sort(eig(Y1_pinv*Y2),'descend');
AOAs = sort(asind(imag(log(z_hat(1:L))) ./ pi)); %% abs NOT NEEDED?

end
