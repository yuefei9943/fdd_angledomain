figure(1)
        plot(hax1, SNRs, snr_RMSE_Phi,'DisplayName', sprintf("N Ant.=%d", N));
        xlabel("SNR")
        ylabel("RMSE Estimated Angle")
        
        figure(2)
        plot(hax2, SNRs, snr_RMSE_Phi,'DisplayName', sprintf("N Ant.=%d", N));
        xlabel("SNR")
        ylabel("RMSE Estimated Large-scale Fading")
    
        log_snr_RMSE_B = 10.*log10(snr_RMSE_B);
        log_snr_RMSE_Phi = 10.*log10(snr_RMSE_Phi);
        
        figure(3)
        plot(hax3, SNRs, log_snr_RMSE_Phi,'DisplayName', sprintf("N Ant.=%d", N));
        xlabel("SNR")
        ylabel("RMSE Estimated Angle (dB)")
        
        plot(hax4, SNRs, log_snr_RMSE_B,'DisplayName', sprintf("N Ant.=%d", N));
        xlabel("SNR")
        ylabel("RMSE Estimated Large-scale Fading (dB)")





    %% ===  Plot Antenna ~ SNR vs Phi    ====
    fig = figure
    xlabel("SNR")
    ylabel("RMSE of Angle Estimation")
    hold on
    iterNAnt = 1;
    for N=NAnt
        plot(SNRs, ant_snr_RMSE_Phi(iterNAnt,:,1),'DisplayName', sprintf("N Ant.=%d", N));
        iterNAnt = iterNAnt + 1;
    end 
    grid on
    title("SNR vs RMSE (Linear Scale) Estimated Angle (Multi-Path)")
    legend show
    hold off
    pngfile=strcat('snr_RMSE_Phi','_',job)
    print(fig,pngfile,'-dpng')

    %% ===  Plot Antenna ~ SNR vs Large-scale Fading Coeff. ===
    fig = figure
    xlabel("SNR")
    ylabel("RMSE of Large-scale Fading Coeff. Estimation")
    hold on
    iterNAnt = 1;
    for N=NAnt
        plot(SNRs, ant_snr_RMSE_B(iterNAnt,:,1),'DisplayName', sprintf("N Ant.=%d", N));
        iterNAnt = iterNAnt + 1;
    end 
    grid on
    title("SNR vs RMSE (Linear Scale) Estimated Large Scale Fading (Multi-Path)")
    legend show
    hold off
    pngfile=strcat('snr_RMSE_B','_',job)
    print(fig,pngfile,'-dpng')
    
    %% ===  Plot Antenna ~ SNR vs Phi.(dB) ===
    fig = figure
    xlabel("SNR")
    ylabel("RMSE of Angle Estimation (dB)")
    hold on
    iterNAnt = 1;
    for N=NAnt
        plot(SNRs, ant_log_snr_RMSE_Phi(iterNAnt,:,1),'DisplayName', sprintf("N Ant.=%d", N));
        iterNAnt = iterNAnt + 1;
    end 
    grid on
    title("SNR vs RMSE (Linear Scale) Estimated Large Scale Fading (Multi-Path)")
    legend show
    hold off
    pngfile=strcat('snr_RMSE_Phi_dB','_',job)
    print(fig,pngfile,'-dpng')

    
    %% ===  Plot Antenna ~ SNR vs Large-scale Fading Coeff (dB). ===
    fig = figure
    xlabel("SNR")
    ylabel("RMSE of Large-scale Fading Coeff. Estimation (dB)")
    hold on
    iterNAnt = 1;
    for N=NAnt
        plot(SNRs, ant_log_snr_RMSE_B(iterNAnt,:,1),'DisplayName', sprintf("N Ant.=%d", N));
        iterNAnt = iterNAnt + 1;
    end 
    grid on
    title("SNR vs RMSE (dB) Estimated Large Scale Fading (Multi-Path)")
    legend show
    hold off
    pngfile=strcat('snr_RMSE_B_dB','_',job)
    print(fig,pngfile,'-dpng')




    %                    fprintf(fileID,"AoA_deg %f\n", AoA_deg);
%                    fprintf(fileID,"AoA_dft_deg %f\n", AoA_dft_deg);
%                    fprintf(fileID,"AoA_music_deg %f\n", AoA_music_deg);
%                    fprintf(fileID,"AoA_matpencil_deg %f\n", AoA_matpencil_deg);