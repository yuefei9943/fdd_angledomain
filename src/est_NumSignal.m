function varargout=est_NumSignal(ymk_Sampled)
clc;
clear all;
close all;

load('ymk_Sampled.mat')

[N,K] = size(ymk_Sampled);
nSignals = 11;

dist = zeros(nSignals,1);% Measure of closeness
AIC = zeros(size(dist));
MDL = zeros(size(dist));
for m=0:nSignals-1
    
    % Compute Eigenvalues for rank-deficient matrix
    P = getPencilParam(N,m);
    E_sorted = computeEigVal(ymk_Sampled,N,P)
    % Compute metrics of measures on closeness of eigenvalues
    En = E_sorted(m+1:end);
    avg_G = prod(En)^(1/(N-m));
    avg_A = sum(En)/(N-m);
    
    dist(m+1) = -K*(N-m)*log(avg_G / avg_A);
    AIC(m+1) = 2*dist(m+1) + 2*m*(2*N-m);
    MDL(m+1) = dist(m+1) + 0.5*m*(2*N-m)*log(K);
end
idxAIC = min(AIC);
idxMDL = min(MDL);

figure
grid on
hold on
m = 0:1:10;
plot(m,AIC,'DisplayName','AIC')
plot(m,MDL,'DisplayName','MDL')
legend show
legend('AutoUpdate','off')
plot(m(idxAIC),AIC(idxAIC),'pentagram','MarkerSize',12,'MarkerEdgeColor','#EDB120')
text(m(idxAIC),AIC(idxAIC),strcat('d=',num2str(m(idxAIC))));
plot(m(idxMDL),MDL(idxMDL),'pentagram','MarkerSize',12,'MarkerEdgeColor','#EDB120')
text(m(idxMDL),MDL(idxMDL),strcat('d=',num2str(m(idxMDL))));
title('Measure of closeness of eigenvalues')
hold off
pngfile='MDL_Estimation';
print(fig,pngfile,'-dpng')



function P = getPencilParam(N,M)
    % N: number of antennas (space samples)
    % M: number of signals
    P = ceil(N/2); 
    if (P < M)
        P = M;
    end
end


function Z = computeEigVal(x,N,P)
    % N: number of antennas (space samples)
    % P: pencil parameters
    Y1 = zeros(N-P,P);
    Y2 = zeros(N-P,P);
    for i=1:P
        Y1(:,i) = x(i:N-P-1+i,1);
        Y2(:,i) = x(i+1:N-P+i,1);
    end
    Y1_pinv = (Y1'*Y1)\Y1';
    Z = sort(eig(Y1_pinv*Y2),'descend');
end

end