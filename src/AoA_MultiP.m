% function AoA_MultiP(nthreads, nworkers)
    % 
    %     if nargin < 2
    %         nthreads = 1;
    %         nworkers = 1;
    %     end 
    %     %% >>> Checking parallel job vars
    %     matlab_ncores = feature('numcores')
    %     slurm_ncores_per_task = str2num(getenv('SLURM_CPUS_PER_TASK'))
    %     if (isempty(slurm_ncores_per_task))
    %         slurm_ncores_per_task = 1;
    %         disp('SLURM_CPUS_PER_TASK not set')
    %     end
        
    clc;clear all;close all;
    %% Define Channel Variables
    L = 3;
    NAnt = [16]; %Num of Antennas
    fc = 2e9; fs = fc*10;
    c = 3e8;
    lambda = c/fc; 
    d = lambda/2;      % traditional antenna placing λ/2 for fc = 2GHz 
    eta =pi; %%--> assume to be pi
    minSNR = 1;
    maxSNR = 30;
    SNRs = minSNR:1:maxSNR;
    nSNR_level = length(SNRs);
    rho_UL = 200;
    coeff = sqrt(rho_UL/L); 
%         R_pwr = [100/111 10/111 1/111];
    
    %% Define Environment Setup
    M = 10; % 10 APs
    K = 20; % 20 users
    AoA = sort([pi/2 pi/4 pi/10],'descend');
    AoA = AoA';
    
    %% ==== Get ComputeCanada JobID ======
    job=getenv('SLURM_JOB_ID');
    fileID = fopen('exp_out.txt','w');

    %% To see how varying number of antennas results
    numAntenna = length(NAnt);
    ant_snr_RMSE_Phi_dft            = zeros(numAntenna, nSNR_level, 1);
    ant_snr_RMSE_Phi_music          = zeros(numAntenna, nSNR_level, 1);
    ant_snr_RMSE_Phi_matpencil      = zeros(numAntenna, nSNR_level, 1);
    ant_snr_RMSE_Gamma              = zeros(numAntenna, nSNR_level, L);
    ant_snr_RMSE_Gamma_matpencil    = zeros(numAntenna, nSNR_level, L);  
    
    iterNAnt = 1;
    N = NAnt(iterNAnt);
    n = linspace(0,N-1,N);
    % ******
    % Simulation Scenarios: 1 AP <-> 1 User: 3 Propagation Paths
    % ******
    %% ====  Init Channel params ====

    % Uplink Pilot Signals
    % pilot_UL = L x tau
    tau = K; % coherence time
    pilot_UL = ones(1,tau).*exp(1i*(2*pi*fc*repmat([1:tau]/fs, 1, 1))) ;
    pilot_UL = pilot_UL ./ norm(pilot_UL,2);


    %% ============== Channel Estimation =========
    gSize = 100;
    G = linspace(-pi/N, pi/N, gSize);
    %% ======== Define Num of Monte-Carlo Trials =========
    nChannelSim = 100;
    nLargeScaleSim = 10;
    fprintf(fileID,'======== Start of Simulations =======\n');

    %% ====== Declare Channel Perf. Vars ======     
    snr_RMSE_Phi_dft        = zeros(nSNR_level,1);
    snr_RMSE_Phi_music      = zeros(nSNR_level,1);
    snr_RMSE_Phi_matpencil  = zeros(nSNR_level,1);
    snr_RMSE_Gamma          = zeros(nSNR_level,L);
    snr_RMSE_Gamma_matpencil          = zeros(nSNR_level,L);
    % Num of Samples in Time
    T = 16;
    
    
    %% ====== Monte-Carlo SNR Sims =====
    for iterSNR = 1:nSNR_level
        currSNR = SNRs(iterSNR);
        % Steering Vector
        A_signal = sqrt(10^(currSNR/10)) /sqrt(2);
        Amk = zeros(N,L);
        for l=1:L
            Amk(:,l) = exp(1i.*n*eta.* sin(AoA(l)))./sqrt(N);
        end

        perf_RMSE_Phi_dft           = 0;
        perf_RMSE_Phi_music         = 0;
        perf_RMSE_Phi_matpencil     = 0;
        perf_RMSE_Gamma             = zeros(L,1); 
        perf_RMSE_Gamma_matpencil   = zeros(L,1); 

        Amk_rho = coeff*A_signal*Amk;
        Amk_coeff = Amk_rho;
        for p=1:L
            Amk_coeff(:,p) = Amk_coeff(:,p) ./ 10^(p-1);
        end

        PSig = 0;
        PNoise = 0;
        ls_RMSE_Phi_dft = 0;
        ls_RMSE_Phi_music = 0;
        ls_RMSE_Phi_matpencil = 0;
        ls_RMSE_Gamma = zeros(L,1);
        ls_RMSE_Gamma_matpencil = zeros(L,1);
        for iL = 1:nLargeScaleSim
            Bmk = genLargeScaleFading(L);
            for iS = 1:nChannelSim
                alpha = genSmallScaleFading(L);

                nmk = genAWGNCh(N,K,L);
                %% === Sampling ===
                % Amk x Bmk x alpha:    (N,L)x(L,L)x(L,1)
                % pilot_UL:             1 x tau
                % hmk_Sampled:          N x tau
                % nmk_Sampled:          N x tau

                hmk = Amk_coeff*Bmk;
                hmk = hmk*diag(alpha);
                hmk_Sampled = zeros(N,1);
                nmk_Sampled = zeros(N,K);
                for l=1:L
                    hmk_Sampled = hmk_Sampled + hmk(:,l);
                    nmk_Sampled = nmk_Sampled + nmk(:,:,l);
                end
                hmk_Sampled = hmk_Sampled * pilot_UL;
                hmk_processed = hmk_Sampled(:,1:T)*(pilot_UL(1:T)');
                nmk_processed = nmk_Sampled(:,1:T)*(pilot_UL(1:T)');

                ymk = hmk_processed + nmk_processed;
                % T snapshot in Time
                ymk_Sampled = ymk;
                %% Angle of Arrival Estimation
                ymk_stat = sum(ymk_Sampled,2)./T;
                AoA_deg = rad2deg(sort(AoA,'descend'));
                [AoA_hat_DFT] = AoAEstimateMultiP(ymk_stat,G,N,eta,L);
                AoA_dft_deg = rad2deg(AoA_hat_DFT);
                if iS == (nChannelSim - 1)
                    plot_spec = 1;
                    [AoA_music_deg] = music_doa(ymk_Sampled,N,L,plot_spec);
                else 
                    plot_spec = 0;
                    [AoA_music_deg] = music_doa(ymk_Sampled,N,L,plot_spec);         
                end
                [AoA_matpencil_deg] = matpencil_doa(ymk_Sampled,d);
                AoA_hat_matpencil = deg2rad(AoA_matpencil_deg);


                %% Channel Amplitude Estimation
                Amk_hat = zeros(N,L);                                
                for p=1:L
                    Amk_hat(:,p) = exp(-1i.*n'*eta*sin(AoA_hat_DFT(p)))./sqrt(N);
                end

                %% Channel Amplitude Estimation
                Amk_hat_matpencil = zeros(N,L);                                
                for p=1:L
                   Amk_hat_matpencil(:,p) = exp(-1i.*n'*eta*sin(AoA_hat_matpencil(p)))./sqrt(N);
                end
                Dmk_hat = sqrt(L/rho_UL)*pinv(Amk_hat'*Amk_hat)*Amk_hat'*ymk_Sampled;
                Rd_hat = 1/T*((Dmk_hat)*(Dmk_hat'));
                Bmk_hat = sort(diag(sqrt(Rd_hat)),'descend');

                Dmk_hat_matpencil = sqrt(L/rho_UL)*pinv(Amk_hat_matpencil'*Amk_hat_matpencil)*Amk_hat_matpencil'*ymk_Sampled;
                Rd_hat_matpencil = 1/T*((Dmk_hat_matpencil)*(Dmk_hat_matpencil'));
                Bmk_hat_matpencil = sort(diag(sqrt(Rd_hat_matpencil)),'descend');

                %% Performance Evaluation
                [RMSE_Phi_dft, RMSE_Gamma] = CSI_perfEval(AoA_deg,AoA_dft_deg,Amk,Amk_hat,Bmk,Bmk_hat,alpha);
                [RMSE_Phi_music] = CSI_perfEval(AoA_deg,AoA_music_deg');
                [RMSE_Phi_matpencil,RMSE_Gamma_matpencil] = CSI_perfEval(AoA_deg,AoA_matpencil_deg,Amk,Amk_hat_matpencil,Bmk,Bmk_hat_matpencil,alpha);

                perf_RMSE_Phi_dft = perf_RMSE_Phi_dft + RMSE_Phi_dft;
                perf_RMSE_Phi_music = perf_RMSE_Phi_music + RMSE_Phi_music;
                perf_RMSE_Phi_matpencil = perf_RMSE_Phi_matpencil + RMSE_Phi_matpencil;
                perf_RMSE_Gamma = perf_RMSE_Gamma + RMSE_Gamma;
                perf_RMSE_Gamma_matpencil = perf_RMSE_Gamma_matpencil + RMSE_Gamma_matpencil;
                PSig = PSig + norm(hmk_processed,2);
                PNoise = PNoise + norm(nmk_processed,2); 
            end
            avg_RMSE_Phi_dft = perf_RMSE_Phi_dft;
            avg_RMSE_Phi_music = perf_RMSE_Phi_music;
            avg_RMSE_Phi_matpencil = perf_RMSE_Phi_matpencil;
            avg_RMSE_Gamma = perf_RMSE_Gamma;
            avg_RMSE_Gamma_matpencil = perf_RMSE_Gamma_matpencil;
        end
        

        %% ====== Recording Perf. for each SNR level =======
        ls_RMSE_Phi_dft = ls_RMSE_Phi_dft+avg_RMSE_Phi_dft;
        ls_RMSE_Phi_music = ls_RMSE_Phi_music+avg_RMSE_Phi_music;
        ls_RMSE_Phi_matpencil = ls_RMSE_Phi_matpencil+avg_RMSE_Phi_matpencil;

        ls_RMSE_Gamma = ls_RMSE_Gamma+avg_RMSE_Gamma;
        ls_RMSE_Gamma_matpencil = ls_RMSE_Gamma_matpencil+avg_RMSE_Gamma_matpencil;
       
    end
    ls_RMSE_Phi_dft = ls_RMSE_Phi_dft / nChannelSim*nLargeScaleSim;
    ls_RMSE_Phi_music = ls_RMSE_Phi_music / nChannelSim*nLargeScaleSim;
    ls_RMSE_Phi_matpencil = ls_RMSE_Phi_matpencil / nChannelSim*nLargeScaleSim;
    ls_RMSE_Gamma = ls_RMSE_Gamma ./ nChannelSim*nLargeScaleSim;
    ls_RMSE_Gamma_matpencil = ls_RMSE_Gamma_matpencil ./ nChannelSim*nLargeScaleSim;
    
    snr_RMSE_Phi_dft(iterSNR) = sqrt(ls_RMSE_Phi_dft / 3);
    snr_RMSE_Phi_music(iterSNR) = sqrt(ls_RMSE_Phi_music / 3);
    snr_RMSE_Phi_matpencil(iterSNR) = sqrt(ls_RMSE_Phi_matpencil / 3);
    snr_RMSE_Gamma(iterSNR) = sqrt(ls_RMSE_Gamma ./ 3);
    snr_RMSE_Gamma_matpencil(iterSNR) = sqrt(ls_RMSE_Gamma_matpencil / 3);
    %% Print Result to File
    fprintf(fileID,"----- SNR: %d iS %d -----\n",currSNR,iS);
    fprintf(fileID,"Averaged Signal Norm: %f\t Noise Norm: %f\n",PSig/nChannelSim*nLargeScaleSim,PNoise/nChannelSim*nLargeScaleSim);
    fprintf(fileID,"snr %d: snr_RMSE_Phi_dft: %f\n", currSNR, snr_RMSE_Phi_dft(iterSNR));
    fprintf(fileID,"snr %d: snr_RMSE_Phi_matpencil: %f\n", currSNR, (iterSNR));
    for p=1:L
        fprintf(fileID,"snr %d: snr_RMSE_Gamma: %f\n", currSNR, snr_RMSE_Gamma(iterSNR,p));
        fprintf(fileID,"snr %d: snr_RMSE_Gamma_matpencil: %f\n", currSNR, snr_RMSE_Gamma_matpencil(iterSNR,p));
    end
    ant_snr_RMSE_Phi_dft(iterNAnt,:,1)             = snr_RMSE_Phi_dft(:,1);
    ant_snr_RMSE_Phi_music(iterNAnt,:,1)           = snr_RMSE_Phi_music(:,1);
    ant_snr_RMSE_Phi_matpencil(iterNAnt,:,1)       = snr_RMSE_Phi_matpencil(:,1);
    for p=1:L
        ant_snr_RMSE_Gamma(iterNAnt,:,p)               = snr_RMSE_Gamma(:,p);
        ant_snr_RMSE_Gamma_matpencil(iterNAnt,:,p)     = snr_RMSE_Gamma_matpencil(:,p);
    end

    %% ===  Plot Antenna ~ SNR vs AoA Estimation  (dB)  ===
    iterNAnt = 1;
    for N=NAnt
        fig = figure
        xlabel("SNR")
        ylabel("RMSE of Angle Estimation $(^o)$",'Interpreter','latex')
        hold on
        plot(SNRs, ant_snr_RMSE_Phi_dft(iterNAnt,:,1),'DisplayName', 'DFT');
        plot(SNRs, ant_snr_RMSE_Phi_music(iterNAnt,:,1),'DisplayName', 'MUSIC');
        plot(SNRs, ant_snr_RMSE_Phi_matpencil(iterNAnt,:,1),'DisplayName', 'Matrix-Pencil');
        grid on
        title(sprintf("Performance Comparison on Angle Estimation (linear) NAnt=%d",N))
        legend show
        hold off
        pngfile=strcat('snr_RMSE_Phi_Ant',num2str(N),'_',job)
        print(fig,pngfile,'-dpng')
        iterNAnt = iterNAnt + 1;
    end 

    fclose(fileID);
    %===  Plot Antenna ~ SNR vs Channel (linear). ===
    for p=1:L
        fig = figure();
        xlabel("SNR")
        ylabel("RMSE of Channel Estimation (linear)")
        hold on
        ii = 1;
        for N=NAnt
            plot(SNRs, ant_snr_RMSE_Gamma(ii,:,p),'-*','DisplayName', sprintf("DFT--N Ant.=%d", N));
            ii = ii + 1;
        end
        ii = 1;
        for N=NAnt
            plot(SNRs, ant_snr_RMSE_Gamma_matpencil(ii,:,p),'DisplayName', sprintf("Matrix-Pencil--N Ant.=%d", N));
            ii = ii + 1;
        end
        grid on
        title_str = ['Channel Est. Path ',num2str(p),'-Normalized $\frac{E_s}{N_0}=$', num2str(10^-(p-1))];
        title(title_str,'Interpreter','latex')
        legend show
        hold off
        pngfile=sprintf('snr_RMSE_dB_Chan_p%d_%s',p,job)
        print(fig,pngfile,'-dpng')
    end
    
%         ===  Plot Antenna ~ SNR vs Channel (dB). ===
%         for p=1:L
%             fig = figure();
%             xlabel("SNR")
%             ylabel("RMSE of Channel Estimation (dB)")
%             hold on
%             ii = 1;
%             for N=NAnt
%                 plot(SNRs, 10*log10(ant_snr_RMSE_Gamma(ii,:,p)),'-*','DisplayName', sprintf("DFT--N Ant.=%d", N));
%                 ii = ii + 1;
%             end
%             ii = 1;
%             for N=NAnt
%                 plot(SNRs, 10*log10(ant_snr_RMSE_Gamma_matpencil(ii,:,p)),'DisplayName', sprintf("Matrix-Pencil--N Ant.=%d", N));
%                 ii = ii + 1;
%             end
%             grid on
%             title_str = ['Channel Est. Path ',num2str(p),'-Normalized $\frac{E_s}{N_0}=$', num2str(10^-(p-1))];
%             title(title_str,'Interpreter','latex')
%             legend show
%             hold off
%             pngfile=sprintf('snr_RMSE_dB_Chan_p%d_%s',p,job)
%             print(fig,pngfile,'-dpng')
%         end

% end 
        
        
function varargout = CSI_perfEval(AoA,AoA_hat,Amk,Amk_hat,Bmk,Bmk_hat,alpha)
%% RMSE for Estimated Angle

RMSE_Phi = sum(abs(AoA - AoA_hat).^2);

varargout{1} = RMSE_Phi;
%% RMSE for Estimated Amplitude
if nargin > 2
    % Amk:      NxL
    % Bmk:      LxL
    % alpha:    Lx1
    % Bmk_hat:  Lx1
    [N,L] = size(Amk);
    true_Gamma = Amk*Bmk*diag(alpha);
    est_Gamma = Amk_hat*diag(Bmk_hat);
    diff_Gamma = true_Gamma - est_Gamma;
    NRMSE = zeros(L,1);
    for p=1:L
        mse = sum((abs(diff_Gamma(:,p)).^2 )) /N;
        norm_Gamma = sum((abs(est_Gamma(:,p)).^2 )) / N;
        NRMSE(p) = mse / norm_Gamma;
    end
    varargout{2} = NRMSE;
end
end
    

function rotMat = rotationMatrix(N,dPhi_l)
ni = linspace(0,N-1,N);
rotVec = exp(1i*dPhi_l.*ni);
rotMat = diag(rotVec);
end

function alpha=genSmallScaleFading(L)
alpha = sqrt(1/2).*(randn(L,1) + 1i.*randn(L,1));
alpha = sort(alpha,'descend');
end

%% Large-scale Fading -- log-normal model
% For Current Simulation, one AP <-> one User
function Bmk = genLargeScaleFading(L)
sigma = 3;
n = 3.25;
d0 = 1000;
d = rand(L,1);
lambda = 1.5;
PL = -20*log10(lambda/(4*pi*d0)) + 10*n.*log10(d);
PL = PL + sigma*randn(size(d));
B_LS = sort(20.^(-PL./20),'descend');
Bmk = diag(sqrt(B_LS));
end
    
        
function nmk = genAWGNCh(N,K,L)
%% ===== 1 AP <----hmk----> 1 User Sim =====
%% N = number of antenna 
%% K = number of users / length of pilot symbols
nmk = (randn(N,K,L)+1i*randn(N,K,L))./sqrt(2);

end
        
        
        
%% ===== Define NxN DFT =====
function w = dftmat(N)
for k=0:N-1
    for l=0:N-1
        w(k+1,l+1)=exp(-1i*2*pi*k*l/N);
    end
end
w = w./(sqrt(N));
end
        
        
        
function [AoA_Est_DFT] = AoAEstimateMultiP(ymk_Sampled,G,N,eta,L)
%% ==== Extended DFT + angle-rotation-based Multipath Component Est. =====
% Input:    Y_bar, L G and Lamba
% Output:   AOA_estimated, beta_estimated
T = 16;
FN = dftmat(16);
[~,gSize] = size(G);

AoA_Est_DFT = zeros(L,1);
ymk_t = ymk_Sampled;
for l=1:L
    angle_Est = 0;
%     for t=1:T
%         ymk_t = ymk_Sampled(:,t);
        hDFT = FN * ymk_t;

        % Coarse Peak Finding
        % -- Find the central point (qInit) of each path
        [~, qInits] = findMaxL((abs(hDFT).^2),L);
        fNq = FN(qInits(l),:);

        % Finer Peak Finding
        v_curr_best = -inf;
        dg_best = -1;
        for g = 1:gSize
            dPhi_l = G(g);
            rotMat = rotationMatrix(N,dPhi_l);

            S = fNq*rotMat*ymk_t;
            S_val = sum(abs(S).^2,1);

            if (S_val > v_curr_best)
                v_curr_best = S_val;
                dg_best = g;
            end  
        end
        SCorrInit = 2*pi*qInits(l)/(N*eta);

        SCorrShift = SCorrInit- G(dg_best)/eta;
        if SCorrShift > 1
            SCorrShift = 1;
        end

        angle_Est = angle_Est + asin(SCorrShift);
%     end
    AoA_Est_DFT(l) = angle_Est;
    
end
AoA_Est_DFT = sort(AoA_Est_DFT,'descend');
end
        
function [maxVals, maxInds] = findMaxL(X,L)
maxVals = zeros(L,1);
maxInds = zeros(L,1);
for l=1:L
    [maxVals(l),maxInds(l)] = max(X);
    X(maxInds(l)) = -Inf;
end
end
        
function DOAs = music_doa(ymk_Sampled,N,L,plot_spec)
%% AoA estimation using MUSIC
% ymk_Sampled = Nx1
% y = As + n
% N: Number of array elements
% L: Number of paths
ymk_Sampled = ymk_Sampled(:,1);
Ry = 1/N*(ymk_Sampled*ymk_Sampled'); %sample correlation matrix

[~,E,V] = svd(Ry);
[~, index]= sort(E,'descend');
V_sorted = V(index);
Vn = V_sorted(:,L+1:end);
theta = 0:1:180;
Pmusic = zeros(length(theta),1);
for i=1:length(theta)
    aa = exp(-1i*pi*sind(theta(i)).*([0:1:N-1]'));%Nx1
    Pmusic(i) = 1/(aa'*(Vn*Vn')*aa);
end
[Pmusic_sorted,index] = sort(real(10*log10(Pmusic)),'descend');
pk_locs = theta(index);
%% Choose the top L peaks
DOAs = sort(pk_locs(1:L),'descend');

%                 if plot_spec
%                     figure 
%                     plot(theta, Pmusic, '-b',locs(1:L),pks(1:L),'r*');
%                     hold on
%                     text(locs(1:L)+2*sign(locs(1:L)),pks(1:L),num2str(locs(1:L)'));
%                     xlabel('Angle \theta(degree)');
%                     ylabel('$Spatial Power Spectrum P(\theta), dB$','interpreter','latex')
%                 end
end
        
% function DOAs = esprit_doa(ymk_Sampled,N,L,d)
%     %% AoA Estimation based on ESPRIT
%     % Estimation of signal parameters 
%     % via rotational invariance techniques
%     y = ymk_Sampled(:,1);
%     Ry = y*y';
%     [U,E,V] = svd(Ry);
% 
%     % Obtain orthonormal eigvec of the sources
%     [~,sortInd] = sort(E,'descend');
%     U_sorted = U(sortInd);
%     S = U_sorted(:,1:L);
%     S1 = S(1:N-1,:);
%     S2 = S(2:N,:);
%     P = S1\S2;
%     eigP = sort(eig(P),'descend');
%     w = angle(eigP ./ (2*pi*d));
%     DOAs = asind(w);
% end
        
function DOAs = matpencil_doa(ymk_Sampled,d)
M = 3;
L = 6; % size of window
[N,~] = size(ymk_Sampled);
assert( (M <= L) && (L<=(N-M)) );

x = ymk_Sampled(:,1); % Kx1=>only one time sample
Y1 = zeros(N-L,L);
Y2 = zeros(N-L,L);
for p=1:L
    Y1(:,p) = x(p:N-L-1+p,1);
    Y2(:,p) = x(p+1:N-L+p,1);
end

Y1_pinv = (Y1'*Y1)\Y1';
z_hat = sort(eig(Y1_pinv*Y2),'descend');
DOAs = asind(abs(imag(log(z_hat(1:M)))) ./ pi); %% abs NOT NEEDED?
DOAs = sort(DOAs,'descend');
end
        
   