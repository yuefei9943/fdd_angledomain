%% === MUltiple SIgnal Classification (MUSIC) ==== 
% 1. A high-resolution direction-finding algorithm based on the
%    eigenvalue decomposition of the sensor covariance matrix 
%    observed at an array.
% 2. It belongs to the family of subspace-based 
%    direction-finding algorithm
function [AoA_Est] = AoA_Music()
    %% Define Channel Variables
    L = 3;
    N = 32;
    fc = 2e9;    
    c = 3e8;
    lambda = c/fc; 
    d = lambda/2;      % traditional antenna placing λ/2 for fc = 2GHz 
    eta =pi; %%--> assume to be pi

    %% Define Environment Setup
    M = 10; % 10 APs
    K = 20; % 20 users


    % ******
    % Simulation Scenarios: 1 AP <-> 1 User: 3 Propagation Paths
    % ******
    %% ====  Init Channel params ====
    % Define Angle of Arrival
    AoA = [pi/10 pi/4 pi/3]; 
    n = linspace(0,N-1,N);

    % Steering Vector
    % Amk = [a(φ1) | a(φ2) | a(φ3)]
    Amk = zeros(N,L);         
    for l=1:L
        Amk(:,l) = exp(1i.*n*eta.* sin(AoA(l)))'./sqrt(N);
    end

    % Uplink Pilot Signals
    rho_UL = 200;
    coeff = sqrt(rho_UL/L); 
    tauP_UL = K;
    T=16;
    SNR=20;
    pilot_UL = ones(1,tauP_UL).*exp(-1i.*2*pi/tauP_UL);% UL pilot signal = C^(1xtau)


    % Noise Generation
    Bmk = genLargeScaleFading(L);
    alpha = genSmallScaleFading(L);
    nmk = genAWGNCh(SNR,rho_UL,N,K,L);

    %% === Sampling ===
    % ymk_Sampled=(N,T);
    hmk_Sampled = coeff*Amk*Bmk*alpha*pilot_UL;
    nmk_Sampled = zeros(N,1);
    for l=1:L
        nmk_Sampled = nmk_Sampled + nmk(:,:,l);
    end

    ymk = hmk_Sampled + nmk_Sampled;

    % Num of subarrays
    nEle = 3;
    J = N/nEle;
    % For the pth subarray, the source signal arrival matrix is:
    A1 = Amk; 
    P = diag( exp(1i*eta.*cos(AoA)) );
    
    %% Sample sensor covariance matrix
    Ry = ymk * ymk';
    % P = 3 x 3
    % Ry = 32 x 32
    % 3 x 3 
    Rsmooth = 0;
    for j=1:J
        Pj = P^(j-1);
        Rsmooth = Rsmooth + Pj.*Ry.*Pj';
    end
    Rsmooth = Rsmooth / J;

    [V, D] = eig(R);
    noiseSub = V(:, 1:N-L); % Noise subspace of R

    thetaGrid    = 0:1:180;
    a = zeros(N,length(thetaGrid));
    corrVal = zeros(length(thetaGrid),1);
    for theta = 1:length(thetaGrid)
        a(:,theta) = exp(-1i*eta.*[0:N-1]');
        corrVal(theta,1) = 1/(norm(a(:,theta)'*noiseSub').^2);
    end
    [corrValSorted, orgInd] = sort(corrVal, 'descend');

    DoA = orgInd(1:L,1)
    fig1=figure(1)
    plot(corrValSorted);
    xlabel('Angle (deg)');
    ylabel('1/Norm^2');
    title ('Estimation of DOAs over the differenet sources amplitude variances')
    grid on;

    print(fig1,'MUSIC_AoA','-dpng')

end


function alpha=genSmallScaleFading(L)
    alpha = sqrt(1/2).*(randn(L,1) + 1i.*randn(L,1));
end


function Bmk = genLargeScaleFading(L)
    %% Large-scale Fading -- log-normal model
    % Bmk => (L,L);
    z_sigma = 8;
    Bandwidth = 100e6;
    noiseFigure = 5; %dB
    noiseFloordBm =  -174+10*log10(Bandwidth) + noiseFigure;
    pentrationlossdB = 20; %dB
    d_LS = -148 - 35.*log10(0.7265) - noiseFloordBm - pentrationlossdB;
    Z_LS = z_sigma/sqrt(2).*(randn(L,1) + 1i.*randn(L,1));
    B_LS = 10 .^((d_LS + Z_LS)./10);
    Bmk = diag(sqrt(B_LS));
end


function nmk = genAWGNCh(SNR,rho_UL,N,K,L)
    %% ===== 1 AP <----hmk----> 1 User Sim =====
    % var_AWGN=P_AWGN ~ func{SNR and UL signal power}
    P_AWGN = rho_UL / 10^(SNR/10);
    nmk = sqrt(P_AWGN/2).*(randn(N,K,L)+1i*randn(N,K,L));
end